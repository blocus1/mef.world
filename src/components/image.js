import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'

const Image = () => {
  const {
    placeholderImage: {
      childrenImageSharp: [image],
    },
  } = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "gatsby-astronaut.png" }) {
        childrenImageSharp {
          gatsbyImageData(placeholder: BLURRED, quality: 100)
        }
      }
    }
  `)

  return <GatsbyImage image={getImage(image)} alt='Ahmed Meftah' placeholder='blurred' />
}

export default Image
